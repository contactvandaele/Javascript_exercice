var mots = [

    {

        terme: "Procrastination",

        definition: "Tendance pathologique à remettre systématiquement au lendemain"

    },

    {

        terme: "Tautologie",

        definition: "Phrase dont la formulation ne peut être que vraie"

    },

    {

        terme: "Oxymore",

        definition: "Figure de style qui réunit dans un même syntagme deux termes sémantiquement opposés"

    }

];


baliseDl = document.createElement("dl");
baliseDl.id = "dl";
document.getElementById("contenu").appendChild(baliseDl);

mots.forEach(function(mot){
    console.log(mot);
    baliseDt = document.createElement("dt");
    baliseDd = document.createElement("dd");
    baliseDt.textContent = mot.terme;
    baliseDd.textContent = mot.definition;
    document.getElementById("dl").appendChild(baliseDt);
    document.getElementById("dl").appendChild(baliseDd);
})
