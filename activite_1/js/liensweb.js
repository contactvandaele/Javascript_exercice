/* 
Activité 1
*/

// Liste des liens Web à afficher. Un lien est défini par :
// - son titre
// - son URL
// - son auteur (la personne qui l'a publié)
var listeLiens = [
    {
        titre: "So Foot",
        url: "http://sofoot.com",
        auteur: "yann.usaille"
    },
    {
        titre: "Guide d'autodéfense numérique",
        url: "http://guide.boum.org",
        auteur: "paulochon"
    },
    {
        titre: "L'encyclopédie en ligne Wikipedia",
        url: "http://Wikipedia.org",
        auteur: "annie.zette"
    }
];

// TODO : compléter ce fichier pour ajouter les liens à la page web


  //  Le titre de chaque lien est cliquable et envoie vers son URL.                                                   REPSECTE
  //  La couleur à donner au titre d’un lien est “#428bca”.                                                           RESPECTE
  //  Les fichiers liensweb.css et liensweb.html ne doivent pas être modifiés.                                        RESPECTE
  //  Conformément aux bonnes pratiques vues dans le cours,                                                           RESPECTE
        // les nouveaux éléments du DOM doivent être créés et modifiés avant d’être ajoutés à la page.                RESPECTE
  //  Les variables JavaScript doivent respecter la norme camelCase                                                   RESPECTE
        //et le fichier liensweb.js doit être correctement indenté.                                                   RESPECTE


listeLiens.forEach(function(link){

    //on crée d'abord une div qui contiendra nos futurs elements
    divLien = document.createElement("div");
    //on donne la classe lien qui contient le design CSS
    divLien.className = "lien";
    //on ajoute cette div et on recupere son noeud
    var eltAjoute = document.getElementById("contenu").appendChild(divLien);

    // creation de l'element lien auquel on vient ajouté le lien et le texte (on ajoute aussi la couleur demandé)
    lien = document.createElement("a");
    lien.href = link.url;
    lien.textContent = link.titre;
    lien.style.color = "#428bca";

    //Creation des spans

    //premier span 
    spanUrl = document.createElement("span");
    spanUrl.textContent = " " + link.url;
    //deuxieme span
    spanAuteur = document.createElement("span");
    spanAuteur.textContent = "Ajouté  par " + link.auteur;

    //on crée une balise de passage a la ligne qui sera inséré pour la mise en forme
    passageLigne = document.createElement("br");

    //nous pouvons desormais ajouté les differentes informations
    eltAjoute.appendChild(lien);
    eltAjoute.appendChild(spanUrl);
    eltAjoute.appendChild(passageLigne);
    eltAjoute.appendChild(spanAuteur);
});
