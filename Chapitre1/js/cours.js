
if (document.body.nodeType === document.ELEMENT_NODE) {

    console.log("Body est un noeud élément");

} else {

    console.log("Body est un noeud textuel");

}

// Affiche les noeuds enfant du noeud body

for (var i = 0; i < document.body.childNodes.length; i++) {

    console.log(document.body.childNodes[i]);

}