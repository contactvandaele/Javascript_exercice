var domCollections = document.getElementsByTagName("a");
console.log(domCollections.length);
console.log(domCollections[0].getAttribute("href"));
console.log(domCollections[domCollections.length-1].getAttribute("href"));


function possede(id, classe){
    var instrument = document.getElementById(id);
    if (instrument !== null) {
        console.log(instrument.classList.contains(classe));
    } else {
        console.log("Aucun élément ne possède l'identifiant " + id);
}
}

possede("saxophone", "bois"); // Doit afficher true

possede("saxophone", "cuivre"); // Doit afficher false

possede("trompette", "cuivre"); // Doit afficher true

possede("contrebasse", "cordes"); // Doit afficher une erreur

