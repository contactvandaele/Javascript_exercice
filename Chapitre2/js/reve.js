// TODO : écrire la fonction compterElements
function compterElements(balise){
	return document.querySelectorAll(balise).length;
}

console.log(compterElements("p")); // Doit afficher 4

console.log(compterElements(".adjectif")); // Doit afficher 3

console.log(compterElements("p .adjectif")); // Doit afficher 3

console.log(compterElements("p > .adjectif")); // Doit afficher 2