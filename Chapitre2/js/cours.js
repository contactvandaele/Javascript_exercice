// Tous les paragraphes

console.log(document.querySelectorAll("p").length); // Affiche 3


// Tous les paragraphes à l'intérieur de l'élément identifié par "contenu"

console.log(document.querySelectorAll("#contenu p").length); // Affiche 2


// Tous les éléments ayant la classe "existe"

console.log(document.querySelectorAll(".existe").length); // Affiche 8