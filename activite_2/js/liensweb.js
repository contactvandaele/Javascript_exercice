/* 
Activité 1
*/

// Liste des liens Web à afficher. Un lien est défini par :
// - son titre
// - son URL
// - son auteur (la personne qui l'a publié)
var listeLiens = [
    {
        titre: "So Foot",
        url: "http://sofoot.com",
        auteur: "yann.usaille"
    },
    {
        titre: "Guide d'autodéfense numérique",
        url: "http://guide.boum.org",
        auteur: "paulochon"
    },
    {
        titre: "L'encyclopédie en ligne Wikipedia",
        url: "http://Wikipedia.org",
        auteur: "annie.zette"
    }
];

// Crée et renvoie un élément DOM affichant les données d'un lien
// Le paramètre lien est un objet JS représentant un lien
function creerElementLien(lien) {
    var titreLien = document.createElement("a");
    titreLien.href = lien.url;
    titreLien.style.color = "#428bca";
    titreLien.style.textDecoration = "none";
    titreLien.style.marginRight = "5px";
    titreLien.appendChild(document.createTextNode(lien.titre));

    var urlLien = document.createElement("span");
    urlLien.appendChild(document.createTextNode(lien.url));

    // Cette ligne contient le titre et l'URL du lien
    var ligneTitre = document.createElement("h4");
    ligneTitre.style.margin = "0px";
    ligneTitre.appendChild(titreLien);
    ligneTitre.appendChild(urlLien);

    // Cette ligne contient l'auteur
    var ligneDetails = document.createElement("span");
    ligneDetails.appendChild(document.createTextNode("Ajouté par " + lien.auteur));

    var divLien = document.createElement("div");
    divLien.classList.add("lien");
    divLien.appendChild(ligneTitre);
    divLien.appendChild(ligneDetails);

    return divLien;
}


  //  Le formulaire apparaît lors du clic sur le bouton “Ajouter un lien”.                                     REALISE
  //  La saisie des champs Titre, URL et Auteur du lien est obligatoire.                                       REALISE
  //  Si l’URL saisie ne commence ni par “http://” ni par “https://”, on lui ajoute “http://” au début.        REALISE 
  //  Lorsque l’utilisateur valide le nouveau lien, celui-ci est ajouté en haut de la page, le formulaire      REALISE
  //     d’ajout disparaît et un message d’information s’affiche pendant 2 secondes.                           REALISE
  //  Les variables JavaScript doivent respecter la norme camelCase et le fichier liensweb.js doit             REALISE 
  //    être correctement indenté.                                                                             REALISE


//Cette fonction permet l'affichage du formulaire et la disparition du bouton "Ajouter un lien"
function afficherFormulaire(){
    formulaire.style.display = "block";
    demandeFormulaire.style.display = "none";
}

//Cette fonction permet la disparition  du formulaire et l'affichage du bouton "Ajouter un lien"
function cacherFormulaire(){
    formulaire.style.display = "none";
    demandeFormulaire.style.display = "block";
}

//Cette fonction permet d'afficher le message en y ajoutant le titre de ce qui a été ajouté
function afficherMessage(titre){
    messageAjout.textContent = "Le lien \"" + titre + "\" a bien été ajouté";
    messageAjout.style.display = "block";
 }

//Cette fonction permet de cacher le message (ce qui a lieu après 2 secondes)
function cacherMessage(){
    messageAjout.style.display = "none";
}

//Cette fonction permet de Valider le formulaire afin d'ajouter le lien
function validerFormulaire(e){
    e.preventDefault();
    //on cree un objet qui nous permettera de reutiliser la fonctioner CreerElementLien
    var lienAjout = {
        titre: titreElt.value,
        url: linkElt.value,
        auteur: auteurElt.value
    }
    //On realise la regex
    var regex = new RegExp("^(http|https)://");
    //On test la regex, si elle ne reponds pas au critères, on modifie l'url en consequence
    if(!regex.test(linkElt.value)){
        lienAjout.url = "http://" + linkElt.value;
    }
    //Sinon on créé l'element et on l'ajoute AVANT les autres
    var nouvelElement = creerElementLien(lienAjout);
    contenu.insertBefore(nouvelElement, contenu.firstChild);
    //On cache le formulaire
    cacherFormulaire();
    //On affiche le message
    afficherMessage(lienAjout.titre);
    //On clear les valeurs
    titreElt.value = "";
    linkElt.value = "";
    auteurElt.value = "";
    //on  cache le message après deux secondes
    setTimeout(cacherMessage, 2000);
}

// dès le debut, on recupere tous les elements dont on aura besoin
var contenu = document.getElementById("contenu");
var demandeFormulaire = document.getElementById("demande_formulaire");
var formulaire = document.getElementById("formulaire");
var messageAjout = document.getElementById("message_ajout");
var linkElt = document.getElementById("link");
var titreElt = document.getElementById("title");
var auteurElt = document.getElementById("author");

// Parcours de la liste des liens et ajout d'un élément au DOM pour chaque lien
listeLiens.forEach(function (lien) {
    var elementLien = creerElementLien(lien);
    contenu.appendChild(elementLien);
});

// on ajoute les evenements sur les differents elements
demandeFormulaire.addEventListener("click", afficherFormulaire);
formulaire.addEventListener("submit", validerFormulaire)
