/* 
Activité 1
*/

// Liste des liens Web à afficher. Un lien est défini par :
// - son titre
// - son URL
// - son auteur (la personne qui l'a publié)
var listeLiens = [
    {
        titre: "So Foot",
        url: "http://sofoot.com",
        auteur: "yann.usaille"
    },
    {
        titre: "Guide d'autodéfense numérique",
        url: "http://guide.boum.org",
        auteur: "paulochon"
    },
    {
        titre: "L'encyclopédie en ligne Wikipedia",
        url: "http://Wikipedia.org",
        auteur: "annie.zette"
    }
];


//   Les liens affichés sont récupérés depuis le serveur.                                                        REALISE   
//   Le nouveau lien n’est affiché sur la page qu’en cas de succès de l’ajout sur le serveur.                    REALISE
//   Le formulaire d’ajout est remplacé par le bouton “Ajouter un lien” quel que soit le résultat                REALISE
//              de l’ajout sur le serveur.                                                                       REALISE
//   Contrairement à l’activité 2, le rechargement de la page web affiche toujours le nouveau lien               REALISE
//              puisque celui-ci est sauvegardé sur le serveur.                                                  REALISE
//   Les communications avec le serveur utilisent les fonctions ajaxGet et ajaxPost définies dans le cours.      REALISE
//   Les variables JavaScript doivent respecter la norme camelCase et le fichier liensweb.js doit                REALISE
//              être correctement indenté.                                                                       REALISE


// Crée et renvoie un élément DOM affichant les données d'un lien
// Le paramètre lien est un objet JS représentant un lien
function creerElementLien(lien) {
    var titreLien = document.createElement("a");
    titreLien.href = lien.url;
    titreLien.style.color = "#428bca";
    titreLien.style.textDecoration = "none";
    titreLien.style.marginRight = "5px";
    titreLien.appendChild(document.createTextNode(lien.titre));

    var urlLien = document.createElement("span");
    urlLien.appendChild(document.createTextNode(lien.url));

    // Cette ligne contient le titre et l'URL du lien
    var ligneTitre = document.createElement("h4");
    ligneTitre.style.margin = "0px";
    ligneTitre.appendChild(titreLien);
    ligneTitre.appendChild(urlLien);

    // Cette ligne contient l'auteur
    var ligneDetails = document.createElement("span");
    ligneDetails.appendChild(document.createTextNode("Ajouté par " + lien.auteur));

    var divLien = document.createElement("div");
    divLien.classList.add("lien");
    divLien.appendChild(ligneTitre);
    divLien.appendChild(ligneDetails);

    return divLien;
}


//Cette fonction permet l'affichage du formulaire et la disparition du bouton "Ajouter un lien"
function afficherFormulaire(){
    formulaire.style.display = "block";
    demandeFormulaire.style.display = "none";
}

//Cette fonction permet la disparition  du formulaire et l'affichage du bouton "Ajouter un lien"
function cacherFormulaire(){
    formulaire.style.display = "none";
    demandeFormulaire.style.display = "block";
}

//Cette fonction permet d'afficher le message en y ajoutant le titre de ce qui a été ajouté
function afficherMessage(titre){
    messageAjout.textContent = "Le lien \"" + titre + "\" a bien été ajouté";
    messageAjout.style.display = "block";
 }

//Cette fonction permet de cacher le message (ce qui a lieu après 2 secondes)
function cacherMessage(){
    messageAjout.style.display = "none";
}

//Cette fonction permet de Valider le formulaire afin d'ajouter le lien
function validerFormulaire(e){
    e.preventDefault();
    //on cree un objet qui nous permettera de reutiliser la fonctioner CreerElementLien
    var lienAjout = {
        titre: titreElt.value,
        url: linkElt.value,
        auteur: auteurElt.value
    }
    //On realise la regex
    var regex = new RegExp("^(http|https)://");
    //On test la regex, si elle ne reponds pas au critères, on modifie l'url en consequence
    if(!regex.test(linkElt.value)){
        lienAjout.url = "http://" + linkElt.value;
    }
    //Sinon on créé l'element et on l'ajoute AVANT les autres

    //On fait une requete POST avec l'objet lienAjout afin de pourvoir l'envoyer au serveur,
    //si la requete aboutie alors la callback ajoute l'element au DOM
    ajaxPost("https://oc-jswebsrv.herokuapp.com/api/lien", lienAjout, function(){
        //on vient creer l'element HTML puis on l'ajoute
        var nouvelElement = creerElementLien(lienAjout);
        contenu.insertBefore(nouvelElement, contenu.firstChild);
        //On affiche le message
        afficherMessage(lienAjout.titre);
        setTimeout(cacherMessage, 2000);
    }, true);

    //On cache le formulaire
    cacherFormulaire();
    //On clear les valeurs
    titreElt.value = "";
    linkElt.value = "";
    auteurElt.value = "";
    //on  cache le message après deux secondes
}


//Fonction qui permet (en cas de succes de la requete pour recuperer les liens)
// d'afficher les liens
function afficherLien(response){
    objJson = JSON.parse(response);
    // Parcours de la liste des liens et ajout d'un élément au DOM pour chaque lien
    objJson.forEach(function (lien) {
        var elementLien = creerElementLien(lien);
        contenu.appendChild(elementLien);
    });
}

// dès le debut, on recupere tous les elements dont on aura besoin
var contenu = document.getElementById("contenu");
var demandeFormulaire = document.getElementById("demande_formulaire");
var formulaire = document.getElementById("formulaire");
var messageAjout = document.getElementById("message_ajout");
var linkElt = document.getElementById("link");
var titreElt = document.getElementById("title");
var auteurElt = document.getElementById("author");

//Ici on requete cette URL en GET pour recuperer les liens, la callback "afficherLien" 
//permet de tout afficher si la requete a abouti
ajaxGet("https://oc-jswebsrv.herokuapp.com/api/liens", afficherLien);

// on ajoute les evenements sur les differents elements
demandeFormulaire.addEventListener("click", afficherFormulaire);
formulaire.addEventListener("submit", validerFormulaire)
